﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace CardScanner_TP

{
    class Client
    {
        static void Main(string[] args)
        {
            string lastCard = "";
            DateTime lastOpenTime = new DateTime();
            string gameCode;
            string URL;

            // Read a particular key from the config file 
            gameCode = ConfigurationManager.AppSettings.Get("gameCode");
            URL = ConfigurationManager.AppSettings.Get("URL");
            Console.WriteLine("The value of gamecode: " + gameCode);
            Console.WriteLine("The value of URL: " + URL);

            // Read all the keys from the config file
            //  NameValueCollection sAll;
            //  sAll = ConfigurationManager.AppSettings;


            Console.ReadLine();
            while (true)
            {
                string cardName = Console.ReadLine();
                DateTime currentOpenTime = DateTime.Now;
                double diff = (currentOpenTime - lastOpenTime).TotalSeconds;
                lastOpenTime = DateTime.Now;
                if (cardName =="" || (diff <  30 && lastCard == cardName))
                {
        
                }
                else
                {
                   
                    CasinoAPIRequest casinoAPIRequest = new CasinoAPIRequest
                    {
                        Cardcode = cardName,
                        Gamecode = gameCode
                    };
                    lastCard = cardName;
  
                   
                  string newResponse = InvokePostMethod(casinoAPIRequest, URL);


                    var objResponse = new JavaScriptSerializer().Deserialize<InsertCardResponse>(newResponse);
                    Console.ResetColor();
                    if (objResponse.Status == "Failure" || objResponse.Status == "Fail")
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine(newResponse);
                    }
                    else
                    {
                        //  Console.WriteLine(response);
                        Console.WriteLine(newResponse);
                    }
                }
            }
        }
        public static string InvokePostMethod(object obj, string strUrl)
        {
            try
            {
                var request = (HttpWebRequest)WebRequest.Create(strUrl);
                request.ContentType = "application/json";
                request.Method = "POST";
                ASCIIEncoding encoder = new ASCIIEncoding();

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(obj);
                    streamWriter.Write(json);
                }
                var response = (HttpWebResponse)request.GetResponse();
                var streamReader = new StreamReader(response.GetResponseStream());

                return streamReader.ReadToEnd();
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            //return errorResponse.StatusCode;
                            return reader.ReadToEnd();
                        }
                    }
                }
            }
            return string.Empty;
        }
    }
    class Request
    {
        public string BarCode;
    }
    public class CasinoAPIRequest
    {
        public string Cardcode { get; set; }
        public string Gamecode { get; set; }
    }

    public class InsertCardResponse
    {
        public string Cardcode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
    }
}

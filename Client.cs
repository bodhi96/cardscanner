using System;

using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace CardScanner_TP

{
    class Client
    {
        private const int WH_KEYBOARD_LL = 13;
        private const int WM_KEYDOWN = 0x0100;
        private static LowLevelKeyboardProc _proc = HookCallback;
        private static IntPtr _hookID = IntPtr.Zero;
        private static string lastCard;


        static void Main(string[] args)
        {
            _hookID = SetHook(_proc);
            string gameCode = ConfigurationManager.AppSettings.Get("gameCode");

            Console.WriteLine("Game Code    " + gameCode);
            Application.Run();
            UnhookWindowsHookEx(_hookID);


            // Read a particular key from the config file 


            // Read all the keys from the config file
            //  NameValueCollection sAll;
            //  sAll = ConfigurationManager.AppSettings;

        }
        private static IntPtr SetHook(LowLevelKeyboardProc proc)
        {
            using (Process curProcess = Process.GetCurrentProcess())
            using (ProcessModule curModule = curProcess.MainModule)
            {
                return SetWindowsHookEx(WH_KEYBOARD_LL, proc,
                    GetModuleHandle(curModule.ModuleName), 0);
            }
        }

        private delegate IntPtr LowLevelKeyboardProc(
            int nCode, IntPtr wParam, IntPtr lParam);

        private static IntPtr HookCallback(
            int nCode, IntPtr wParam, IntPtr lParam)

        {



            if (nCode >= 0 && wParam == (IntPtr)WM_KEYDOWN)
            {

                DateTime lastOpenTime = new DateTime();
                string gameCode;
                string URL;



                int vkCode = Marshal.ReadInt32(lParam);
                bool canInsert = true;
                gameCode = ConfigurationManager.AppSettings.Get("gameCode");
                URL = ConfigurationManager.AppSettings.Get("URL");
                string cardName = ((Keys)vkCode).ToString();


                char[] n = cardName.Substring(1).ToCharArray();

                if (n.Length == 0 && vkCode != 13)
                {
                    cardName = lastCard + ((Keys)vkCode).ToString();
                    canInsert = false;


                }
                else if (vkCode != 13 && canInsert == true)
                {
                    canInsert = false;
                    cardName = lastCard + n[0].ToString();


                }
                if (cardName.Length > 3)
                {
                    canInsert = false;
                }
                else
                {

                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    ClearCurrentConsoleLine();
                    Console.WriteLine(cardName);

                }

                if (!string.IsNullOrEmpty(lastCard) && vkCode == 13)
                {
                    canInsert = true;
                }

                if (canInsert)
                {

                    if (!string.IsNullOrEmpty(lastCard))
                    {


                        cardName = lastCard;
                        DateTime currentOpenTime = DateTime.Now;
                        double diff = (currentOpenTime - lastOpenTime).TotalSeconds;
                        lastOpenTime = DateTime.Now;
                        if (cardName == "")
                        {

                        }
                        else
                        {

                            CasinoAPIRequest casinoAPIRequest = new CasinoAPIRequest
                            {
                                Cardcode = cardName,
                                Gamecode = gameCode
                            };
                            lastCard = cardName;


                            string newResponse = InvokePostMethod(casinoAPIRequest, URL);


                            var objResponse = new JavaScriptSerializer().Deserialize<InsertCardResponse>(newResponse);
                            Console.ResetColor();
                            if (objResponse.Status == "Failure" || objResponse.Status == "Fail")
                            {
                                Console.BackgroundColor = ConsoleColor.White;
                                Console.ForegroundColor = ConsoleColor.Red;
                                Console.WriteLine(newResponse);
                                Console.WriteLine("Enter next card");
                            }
                            else
                            {
                                //  Console.WriteLine(response);
                                Console.WriteLine(newResponse);
                                Console.WriteLine("Enter next card");
                            }
                        }
                        lastCard = "";
                    }

                }
                else { lastCard = cardName; }




            }


            return CallNextHookEx(_hookID, nCode, wParam, lParam);
        }

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowsHookEx(int idHook,
            LowLevelKeyboardProc lpfn, IntPtr hMod, uint dwThreadId);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool UnhookWindowsHookEx(IntPtr hhk);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr CallNextHookEx(IntPtr hhk, int nCode,
            IntPtr wParam, IntPtr lParam);

        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetModuleHandle(string lpModuleName);
        public static string InvokePostMethod(object obj, string strUrl)

        {
            try

            {
                var request = (HttpWebRequest)WebRequest.Create(strUrl);
                request.ContentType = "application/json";
                request.Method = "POST";
                ASCIIEncoding encoder = new ASCIIEncoding();

                using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(obj);
                    streamWriter.Write(json);
                }
                var response = (HttpWebResponse)request.GetResponse();
                var streamReader = new StreamReader(response.GetResponseStream());

                return streamReader.ReadToEnd();
            }
            catch (WebException wex)
            {
                if (wex.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)wex.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            //return errorResponse.StatusCode;
                            return reader.ReadToEnd();
                        }
                    }
                }
            }
            return string.Empty;
        }
        public static void ClearCurrentConsoleLine()
        {
            int currentLineCursor = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentLineCursor);
        }
    }
    class Request
    {
        public string BarCode;
    }
    public class CasinoAPIRequest
    {
        public string Cardcode { get; set; }
        public string Gamecode { get; set; }
    }

    public class InsertCardResponse
    {
        public string Cardcode { get; set; }
        public string Status { get; set; }
        public string ErrorMessage { get; set; }
    }
}
